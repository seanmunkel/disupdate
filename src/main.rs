use std::default::Default;
use std::path::{Path, PathBuf};

use serde::{Serialize, Deserialize};


const DOWNLOAD_URL: &'static str = "https://discord.com/api/download?platform=linux&format=tar.gz";

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
struct Config {
    #[serde(skip_serializing)]
    path: Option<PathBuf>,
    // Below are the actual options. The first one is for convenience
    current_version: Option<String>,
}

fn get_current_download_url() -> Result<String, ureq::Error> {
    Ok(ureq::get(DOWNLOAD_URL).call()?.get_url().to_owned())
}

fn get_latest_downloaded_version() -> Config {
    let home = match std::env::var("HOME") {
        Ok(x) => x,
        Err(_) => {
            eprintln!("No home directory set. No settings will be saved");
            return Default::default();
        }
    };
    let home = Path::new(&home);
    let config_path = home.join(".config/disupdate");
    if !config_path.parent().unwrap().exists() {
        if std::fs::create_dir_all(config_path.parent().unwrap()).is_err() {
            eprintln!(
                "Cannot create settings config file in {:?}. No settings will be saved",
                config_path
            );
        }
        return Default::default();
    } else if config_path.exists() {
        let config_file = std::fs::File::open(&config_path);
        if config_file.is_err() {
            return Config {
                path: Some(config_path),
                ..Default::default()
            }
        }
        match serde_json::from_reader(config_file.unwrap()) {
            Ok(c) => c,
            Err(_) => {
                return Config {
                    path: Some(config_path),
                    ..Default::default()
                }
            }
        }
    } else {
        return Config {
            path: Some(config_path),
            ..Default::default()
        };
    }
}

fn remove_old_version() -> std::io::Result<()> {
    let discord_path = Path::new("/opt/Discord");
    if discord_path.exists() {
        std::fs::remove_dir_all(discord_path)
    } else {
        Ok(())
    }
}

fn download_and_install(url: String, config: &Config) -> anyhow::Result<()> {
    let resp = ureq::get(&url).call()?;
    let mut reader = resp.into_reader();
    let mut destination = tempfile::NamedTempFile::new()?;

    std::io::copy(&mut reader, &mut destination)?;
    println!("Downloaded update discord release");

    let out = std::process::Command::new("tar")
        .args(&["xzf", destination.path().to_str().unwrap(), "-C", "/opt/"])
        .output()?;
    if out.status.success() {
        println!("Discord successfully updated");
        let new_config = Config {
            current_version: Some(url),
            ..config.clone()
        };
        if let Some(config_path) = &config.path {
            let body = serde_json::to_string_pretty(&new_config)?;
            std::fs::write(config_path, body)?;
        }
    } else {
        eprintln!("Error while extracting discord update.");
    }
    Ok(())
}

fn main() -> anyhow::Result<()> {
    let config = get_latest_downloaded_version();
    println!("{:?}", config);
    if config.current_version.is_none() {
        println!("No downloaded version detected");
    }
    let url = get_current_download_url().unwrap();
    if config.current_version.as_ref() == Some(&url) {
        println!("Discord is up to date!");
        return Ok(());
    }
    if remove_old_version().is_err() {
        println!("Unable to remove old version.");
        return Ok(());
    }
    download_and_install(url, &config)
}
